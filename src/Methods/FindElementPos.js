import React from 'react'

export default function FindElementPos(nodeRef) {
    const rect = nodeRef.getBoundingClientRect();
      const top = Math.floor(rect.top);
      const left = Math.floor(rect.left);
    // console.log(top, bottom, left, right);
    const newPosition = {
      left,
      top,
    }

    return newPosition;
}
