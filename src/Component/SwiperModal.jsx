import React, { useState, useEffect, useRef, createRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faCircleCheck,
  faPlay,
  faVolumeHigh,
  faVolumeMute,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { VIDEO_URL } from "../config";
import { register } from "swiper/element/bundle";
import axios from "axios";

import Modal from "react-modal";

const customStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
    zIndex: 100000000000,
    overflow: "hidden",
    // height: "-webkit-fill-available",
    height:'100%'
  },
  content: {
    top: "0%",
    left: "0%",
    right: "0%",
    bottom: "0%",
    padding: 0,
    backgroundColor: "transparent",
    border: "1ps solid transparent",
    overflow: "hidden",
    height: "100%",
    zIndex: 10,
  },
};

const productModalStyle = {
  products: {
    height: "fit-content",
    padding: "8px",
    cursor: "pointer",
    borderRadius: "5px",
    background: "rgba(0,0,0,0.7)",
    color: "white",
    fontSize: "16px",
    display: "flex",
    gap: "10px",
  },
};

register();

Modal.setAppElement("#productRoot");

function SwiperModal(prop) {
  const {
    isModalOpen,
    productId,
    setIsModalOpen,
    productVideoResponse,
    viewportHeigth,
    controlsVideoRef,
    currentIndex,
    setCurrentIndex,
    trackEvent,
    setShowThumbnail,
    descriptionBoxRef,
    playlistID,
    setViewportHeight,
    currentVideo,
    setCurrentVideo,
    checkoutEnabled,
    setCheckoutEnabled,
    addToCartEnabled,
    setAddToCartEnabled,
    checkoutType,
    throughDraggable,
    throughSlider,
    handleCloseModal
  } = prop;

  const [selectedProductIndex, setSelectedProductIndex] = useState(0);
  const [selectedVariantIndex, setSelectedVariantIndex] = useState(0);
  const [moreDes, setMoreDes] = useState(false);
  const [varinatSelected, setVariantSelected] = useState(null);
  const [toggleDescription, setToggleDescription] = useState(false);
  const [isMute, setIsMute] = useState(false);
  const [cartCount, setCartCount] = useState(0);
  const [videoWidth, setVideoWidth] = useState(0);

  const ModalSwiperRef = useRef();
  const ProductSwiperRef = useRef();
  const SubVideoSwiperRef = useRef([]);
  const childModalRef = useRef(null);
  const playButtonRef = useRef([]);
  const descriptionTextRef = useRef(null);
  const checkoutGokwikRef = useRef(null);

  if (SubVideoSwiperRef.current.length === 0) {
    SubVideoSwiperRef.current = productVideoResponse?.map((item) =>
      createRef()
    );
    playButtonRef.current = productVideoResponse?.map((item) => createRef());
  }

  useEffect(() => {
    const updateVideWidth = () => {
      setVideoWidth(
        SubVideoSwiperRef.current[currentIndex]?.current?.offsetWidth
      );
    };

    window.addEventListener("resize", updateVideWidth);

    return () => window.removeEventListener("resize", updateVideWidth);
  });

  useEffect(() => {
    setTimeout(() => {
      document.body.style.overflow = "hidden";

      if (ModalSwiperRef.current) {
        // Start: Modal Swiper
        const swiperParams = {
          on: {
            init() {
              // ...
            },
            slideChangeTransitionEnd: function (event) {
              if(throughDraggable){
              const previousIndex = event.previousIndex
              const video = SubVideoSwiperRef.current[previousIndex].current;
            // console.log("Popup Played Video Time");
            // console.log({
            //   store: window.location.hostname,
            //   playlistId: playlistID,
            //   videoId: video.dataset.id,
            //   videoTitle: video.getAttribute('name'),
            //   playedTime: `${Math.round(video.currentTime)}s`
            // });
            trackEvent("Popup Played Video Time", {
              props: {
                  store: window.location.hostname,
                  playlistId: playlistID,
                  videoId: video.dataset.id,
                  videoTitle: video.getAttribute('name'),
                  playedTime: `${Math.round(video.currentTime)}s`
                },
            });
          }

          if(throughSlider){
            const previousIndex = event.previousIndex
              const video = SubVideoSwiperRef.current[previousIndex].current;
            // console.log("PDP Played Video Time");
            // console.log({
            //   store: window.location.hostname,
            //   playlistId: playlistID,
            //   videoId: video.dataset.id,
            //   videoTitle: video.getAttribute('name'),
            //   playedTime: `${Math.round(video.currentTime)}s`
            // });
            trackEvent("PDP Played Video Time", {
              props: {
                  store: window.location.hostname,
                  playlistId: playlistID,
                  videoId: video.dataset.id,
                  videoTitle: video.getAttribute('name'),
                  playedTime: `${Math.round(video.currentTime)}s`
                },
            });
          }

              setToggleDescription(false);
              setMoreDes(false);
              setVariantSelected(null); //Variant Selected is null on Change of Video Slider
              setIsMute(false); //On Change Video Is Not Muted

              if (window.innerWidth < 600) {
                ModalSwiperRef.current.swiper.navigation.prevEl.style.display =
                  "block";
                ModalSwiperRef.current.swiper.navigation.nextEl.style.display =
                  "block";
              }

              descriptionBoxRef.current.forEach((descRef) => {
                descRef.current.classList.add("bringin-hidden");
              });

              event.el.childNodes.forEach((item, index) => {
                if (item.tagName.toUpperCase() === "SWIPER-SLIDE") {
                  if (item.classList.contains("swiper-slide-active")) {
                    setCurrentIndex(index); // Use Effect is called
                  }
                }
              });

              document
                .getElementById("productSwiper")
                .classList.remove("bringin-hidden"); //Product Slider is shown
              ProductSwiperRef.current.swiper.slideTo(0, false); //Slided to zeroth index
            },
            destroy(event){
              if(throughDraggable){
              const activeVideoIndex = event.activeIndex
              const video = SubVideoSwiperRef.current[activeVideoIndex].current;
              // console.log("Popup Played Video Time");
              // console.log({
              //   store: window.location.hostname,
              //   playlistId: playlistID,
              //   videoId: video.dataset.id,
              //   videoTitle: video.getAttribute('name'),
              //   playedTime: `${Math.round(video.currentTime)}s`
              // });
              trackEvent("Popup Played Video Time", {
                props: {
                    store: window.location.hostname,
                    playlistId: playlistID,
                    videoId: video.dataset.id,
                    videoTitle: video.getAttribute('name'),
                    playedTime: `${Math.round(video.currentTime)}s`
                  },
              });
            }

            if(throughSlider){
              const activeVideoIndex = event.activeIndex
              const video = SubVideoSwiperRef.current[activeVideoIndex].current;
              // console.log("PDP Played Video Time");
              // console.log({
              //   store: window.location.hostname,
              //   playlistId: playlistID,
              //   videoId: video.dataset.id,
              //   videoTitle: video.getAttribute('name'),
              //   playedTime: `${Math.round(video.currentTime)}s`
              // });
              trackEvent("PDP Played Video Time", {
                props: {
                    store: window.location.hostname,
                    playlistId: playlistID,
                    videoId: video.dataset.id,
                    videoTitle: video.getAttribute('name'),
                    playedTime: `${Math.round(video.currentTime)}s`
                  },
              });
            }
              
            }
          },
        };

        // now we need to assign all parameters to Swiper element
        Object.assign(ModalSwiperRef.current, swiperParams);

        // and now initialize it
        ModalSwiperRef.current.initialize();

        // Current Video is played and Slided to the clicked thumbanail slide
        ModalSwiperRef.current.swiper.slideTo(currentIndex, false);
        SubVideoSwiperRef.current[currentIndex].current.play();

        // Setting the width of the product slider
        if (window.innerWidth < 1024) {
          document.getElementById(
            "productSwiper"
          ).style.width = `${window.innerWidth}px`;
        } else {
          document.getElementById("productSwiper").style.width = `${
            window.innerWidth * 0.33
          }px`;
        }

        // Changing color of Next and Prev Btn

        ModalSwiperRef.current.swiper.navigation.prevEl.style.color = "white";
        ModalSwiperRef.current.swiper.navigation.nextEl.style.color = "white";

        // End:- Modal swiper

        //Start: -- Product Swiper
        if (ProductSwiperRef.current) {
          const swiperParams = {
            breakpoints: {
              0: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
              350: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
            },
            on: {
              init() {
                // ...
              },
              slideChangeTransitionEnd: function (event) {
                let videoDetailsStr;
                event?.el?.childNodes?.forEach((item) => {
                  if (item?.dataset?.videodetails) {
                    videoDetailsStr = item.dataset.videodetails;
                  }
                });

                if (videoDetailsStr) {
                  const videoDetailsJSON = JSON.parse(videoDetailsStr);

                  if (throughDraggable) {
                    // console.log("Popup Product Slider Click");
                    // console.log({
                    //   store: window.location.hostname,
                    //   productId,
                    //   videoId: videoDetailsJSON?.id,
                    //   videoTitle: videoDetailsJSON?.title,
                    // });
                    trackEvent("Popup Product Slider Click", {
                      props: {
                        store: window.location.hostname,
                        productId,
                        videoId: videoDetailsJSON?.id,
                        videoTitle: videoDetailsJSON?.title,
                      },
                    });
                  }

                  if(throughSlider){
                  //   console.log("PDP Product Slider Click");
                  // console.log({
                  //   store: window.location.hostname,
                  //   productId,
                  //   videoId: videoDetailsJSON?.id,
                  //   videoTitle: videoDetailsJSON?.title,
                  // });
                  trackEvent("PDP Product Slider Click", {
                    props: {
                      store: window.location.hostname,
                      productId,
                      videoId: videoDetailsJSON?.id,
                      videoTitle: videoDetailsJSON?.title,
                    },
                  });
                  }
                }
              },
            },
          };

          // now we need to assign all parameters to Swiper element
          Object.assign(ProductSwiperRef.current, swiperParams);

          // and now initialize it
          ProductSwiperRef.current.initialize();

          ProductSwiperRef.current.swiper.navigation.prevEl.style.color =
            "white";
          ProductSwiperRef.current.swiper.navigation.nextEl.style.color =
            "white";
          ProductSwiperRef.current.swiper.navigation.nextEl.style.setProperty(
            "--swiper-navigation-size",
            "12px"
          );
          ProductSwiperRef.current.swiper.navigation.prevEl.style.setProperty(
            "--swiper-navigation-size",
            "12px"
          );
          ProductSwiperRef.current.swiper.navigation.prevEl.style.fontWeight =
            "bold";
          ProductSwiperRef.current.swiper.navigation.nextEl.style.fontWeight =
            "bold";
        }
        //End: -- Product Swiper

        //Start:-- Setting the Video Width
        SubVideoSwiperRef?.current[currentIndex]?.current?.addEventListener(
          "loadedmetadata",
          () => {
            if (
              SubVideoSwiperRef?.current[currentIndex]?.current?.offsetWidth
            ) {
              const width =
                SubVideoSwiperRef.current[currentIndex].current.offsetWidth;
              setVideoWidth(width);
            }
          }
        );
        // End:-- Setting the Video Width
      }
    }, 0);
  }, []);

  useEffect(() => {
    const updateVideWidth = () => {
      setVideoWidth(
        SubVideoSwiperRef.current[currentIndex]?.current?.offsetWidth
      );
    };

    window.addEventListener("resize", updateVideWidth);

    return () => window.removeEventListener("resize", updateVideWidth);
  });

  useEffect(() => {
    // Playing the current window and Pausing rest
    if (currentIndex !== null) {
      // Setting the video
      const currentVid = productVideoResponse[currentIndex];
      setCurrentVideo({ ...currentVid });

      // Hiding the play button;
      playButtonRef?.current[currentIndex]?.current?.classList?.add("bringin-hidden");

      SubVideoSwiperRef.current.forEach((video, index) => {
        if (index === currentIndex) {
          video.current?.play();
        } else {
          if (video.current) video.current.currentTime = 0;
          video.current?.pause();
        }
      });

      if (SubVideoSwiperRef?.current[currentIndex]?.current?.offsetWidth) {
        const width =
          SubVideoSwiperRef.current[currentIndex].current.offsetWidth;
        setVideoWidth(width);
      }

      ProductSwiperRef.current?.swiper?.updateSlides(); //Update the slides
    }
  }, [currentIndex]);

  useEffect(() => {
    if (currentVideo) {
      // Plausible
      if(throughDraggable){

      // console.log("Popup Video Click");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("Popup Video Click", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
              
    }

    if(throughSlider){
      // console.log("PDP Video Click");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("PDP Video Click", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }
    }
  }, [currentVideo]);

  useEffect(() => {
    if (varinatSelected) {
      const currentProduct =
        productVideoResponse[currentIndex].products[selectedProductIndex];

      if(throughDraggable){
      // console.log("Popup Add to Cart");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   currentProductId: currentProduct?.product_id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      // });
      trackEvent("Popup Add to cart", {
        props: {
          store: window.location.hostname,
          productId,
          currentProductId: currentProduct?.product_id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndex]?.sku,
        },
      });
    }

    if(throughSlider){
      // console.log("PDP Add to Cart");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   currentProductId: currentProduct?.product_id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      // });
      trackEvent("PDP Add to cart", {
        props: {
          store: window.location.hostname,
          productId ,
          currentProductId: currentProduct?.product_id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndex]?.sku,
        },
      });
    }
    }
  }, [varinatSelected]);

  useEffect(() => {
    if (!moreDes && descriptionTextRef.current) {
      // descriptionTextRef.current.scroll({
      //   top: 0,
      //   behavior: 'smooth'
      // })
    }
  }, [moreDes]);

  const handleOpenDescription = (event, index) => {
    event.preventDefault();
    setSelectedProductIndex(index);
    if (window.innerWidth < 600) {
      ModalSwiperRef.current.swiper.navigation.prevEl.style.display = "none";
      ModalSwiperRef.current.swiper.navigation.nextEl.style.display = "none";
    }
    document.getElementById("productSwiper").classList.add("bringin-hidden");
    descriptionBoxRef.current.forEach((descRef, i) => {
      if (i === currentIndex) {
        descRef.current.classList.remove("bringin-hidden");
        descRef.current.style.width = `${SubVideoSwiperRef.current[currentIndex].current.clientWidth}px`;
      } else {
        descRef.current.classList.add("bringin-hidden");
      }
    });

    const currentProduct = productVideoResponse[currentIndex]?.products[index];
    if(throughDraggable){
    // console.log("Popup Product Detail Open");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    //   videoId: currentVideo?.id,
    //   videoTitle: currentVideo?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[0]?.sku,
    // });
    trackEvent("Popup Product Detail Open", {
      props: {
        store: window.location.hostname,
        productId,
        videoId: currentVideo?.id,
        videoTitle: currentVideo?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[0]?.sku,
      },
    });
    }

    if(throughSlider){
    //   console.log("PDP Product Detail Open");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    //   videoId: currentVideo?.id,
    //   videoTitle: currentVideo?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[0]?.sku,
    // });
    trackEvent("PDP Product Detail Open", {
      props: {
        store: window.location.hostname,
        productId,
        videoId: currentVideo?.id,
        videoTitle: currentVideo?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[0]?.sku,
      },
    });
    }
  };

  const handleCloseDesc = (event) => {
    event.preventDefault();

    if (window.innerWidth < 600) {
      ModalSwiperRef.current.swiper.navigation.prevEl.style.display = "block";
      ModalSwiperRef.current.swiper.navigation.nextEl.style.display = "block";
    }

    descriptionBoxRef.current[currentIndex].current.classList.add("bringin-hidden");
    document.getElementById("productSwiper").classList.remove("bringin-hidden");
    setSelectedVariantIndex(0); //Variant is set to default index 0
    setVariantSelected(null); // Variant is set to null
    setToggleDescription(false); //Description Box is Set to Default Mode
  };

  const handleVariantChange = (event, variant, index) => {
    event.target.checked = true;
    setSelectedVariantIndex(index);
  };

  const handleAddToCart = (event, index) => {
    event.preventDefault();
    setToggleDescription(true);
    const variantId =
      productVideoResponse?.[currentIndex]?.products[selectedProductIndex]
        ?.variants[0]?.id;
    document.getElementById(`btnradio_${index}_${variantId}`).checked = true;
  };

  const handleDone = (index) => {
    try {
      const variant =
        productVideoResponse[index]?.products[selectedProductIndex]?.variants[
          selectedVariantIndex
        ];
      const payload = {
        id: variant?.variant_id,
        quantity: 1,
      };

      // Start:-- Testing Mode
      // setToggleDescription(false);
      // setVariantSelected({ ...variant });
      // End:-- Testing Mode

      // Start: Production Mode
      axios
        .post("/cart/add.js", payload)
        .then((res) => {
          if (res?.status === 200) {
            const count = res.data.quantity;
            count && setCartCount(count);
            setToggleDescription(false);
            setVariantSelected({ ...variant });
          }
        })
        .catch((err) => {});
      // End: Production Mode
    } catch (err) {}
  };

  const handleMute = (event, index) => {
    event.preventDefault();
    try {
      if (isMute) {
        if(throughDraggable){
        // console.log("Popup Video Unmuted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("Popup Video Unmuted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });

      }

      if(throughSlider){
        // console.log("PDP Video Unmuted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("PDP Video Unmuted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });
      }


      } else {

        if(throughDraggable){
        // console.log("Popup Video Muted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("Popup Video Muted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });
      }

      if(throughSlider){
        // console.log("PDP Video Muted");
        // console.log({
        //   store: window.location.hostname,
        //   productId,
        //   videoId: currentVideo?.id,
        //   videoTitle: currentVideo.title,
        // });
        trackEvent("PDP Video Muted", {
          props: {
            store: window.location.hostname,
            productId,
            videoId: currentVideo?.id,
            videoTitle: currentVideo.title,
          },
        });
      }
      }
      setIsMute((prev) => !prev);
      SubVideoSwiperRef.current[index].current.muted =
        !SubVideoSwiperRef.current[index].current.muted;
    } catch (err) {}
  };

  const handlePausePlayVideo = (index) => {
    const isPaused = SubVideoSwiperRef.current[currentIndex].current.paused;
    if (isPaused) {
      SubVideoSwiperRef.current[currentIndex].current.play();
      playButtonRef.current[index].current.classList.add("bringin-hidden");
      
      if(throughDraggable){
      // console.log("Popup Video Play");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("Popup Video Play", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }

    if(throughSlider){
      // console.log("PDP Video Play");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("PDP Video Play", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }

    } else {
      SubVideoSwiperRef.current[currentIndex].current.pause();
      if(throughDraggable){
      // console.log("Popup Video Pause");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("Popup Video Pause", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }

    if(throughSlider){
      //  console.log("PDP Video Pause");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      // });
      trackEvent("PDP Video Pause", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
        },
      });
    }
      playButtonRef.current[index].current.classList.remove("bringin-hidden");
    }
  };

  const handleRedirectProductDespcriptionPage = () => {
    const currentProduct =
      productVideoResponse[currentIndex].products[selectedProductIndex];

    if(throughDraggable){
      // console.log("Popup Product Redirect to PDP");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      //   videoId: currentVideo?.id,
      //   videoTitle: currentVideo?.title,
      //   currentProductId: currentProduct?.id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      // });
      trackEvent("Popup Product Redirect to PDP", {
        props: {
          store: window.location.hostname,
          productId,
          videoId: currentVideo?.id,
          videoTitle: currentVideo?.title,
          currentProductId: currentProduct?.id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndex]?.sku,
        },
      });
    }

    if(throughSlider){
    //   console.log("PDP Slider Product Redirect");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    //   videoId: currentVideo?.id,
    //   videoTitle: currentVideo?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[selectedVariantIndex]?.sku,
    // });
    trackEvent("PDP Slider Product Redirect", {
      props: {
        store: window.location.hostname,
        productId,
        videoId: currentVideo?.id,
        videoTitle: currentVideo?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[selectedVariantIndex]?.sku,
      },
    });
    }
   
  };

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={handleCloseModal}
      style={customStyles}
    >
      <section
        style={{ height: `${viewportHeigth}px` }}
        ref={childModalRef}
        className="bringin-relative bringin-w-full xl:bringin-w-[900px] bringin-m-auto"
        id="childModalSwiper"
      >
        <swiper-container
          ref={ModalSwiperRef}
          slides-per-view={"1"}
          spaceBetween={0}
          navigation="true"
          style={{ height: `${viewportHeigth}px` }}
          init="false"
        >
          {productVideoResponse?.map((video, index) => {
            return (
              <swiper-slide style={{width:"830px"}} key={video.id}>
                <div className="bringin-flex bringin-flex-col bringin-justify-center bringin-h-full bringin-items-center bringin-relative">
                  <article className="bringin-absolute bringin-w-full bringin-flex bringin-justify-center bringin-text-white bringin-top-[1%] bringin-z-10">
                    {/* <div className='bringin-flex bringin-justify-between bringin-font-bold bringin-w-[40%] md:bringin-w-[50%] lg:bringin-w-[80%] 3xl:bringin-w-[95%] bringin-text-lg 2xl:bringin-text-3xl'> */}
                    <div
                      ref={controlsVideoRef.current[index]}
                      className="bringin-flex bringin-justify-between bringin-font-bold bringin-px-2  bringin-text-lg 2xl:bringin-text-3xl"
                      style={{ width: `${videoWidth}px` }}
                    >
                      <div
                        className="bringin-cursor-pointer bringin-w-12 bringin-h-12 bringin-rounded-full bringin-bg-black/20 bringin-flex bringin-justify-center bringin-items-center"
                        onClick={(event) => handleMute(event, index)}
                      >
                        {isMute ? (
                          <FontAwesomeIcon icon={faVolumeMute} />
                        ) : (
                          <FontAwesomeIcon icon={faVolumeHigh} />
                        )}
                      </div>
                      <div
                        className="bringin-cursor-pointer bringin-w-12 bringin-h-12 bringin-rounded-full bringin-bg-black/20 bringin-flex bringin-justify-center bringin-items-center"
                        onClick={handleCloseModal}
                      >
                        <FontAwesomeIcon icon={faXmark} />
                      </div>
                    </div>
                  </article>

                  <video
                    name={video?.title}
                    data-id={video?.id}
                    autoPlay={false}
                    ref={SubVideoSwiperRef.current[index]}
                    playsInline
                    className="bringin-h-screen bringin-block bringin-max-w-full bringin-object-contain bringin-overflow-clip"
                    preload="meta"
                    onClick={() => handlePausePlayVideo(index)}
                  >
                    <source
                      src={`${VIDEO_URL}/${video.video_url}`}
                      type="video/mp4"
                    />
                  </video>
                  {/* <article ref={descriptionBoxRef} style={{width:"70%"}} className='bringin-bg-black/70 bringin-absolute bringin-bottom-0 bringin-text-sm bringin-translate-x-20 bringin-rounded-t-xl bringin-pt-5 bringin-pb-12 bringin-pl-4 bringin-pr-4 bringin-text-white  bringin-descriptionBoxModal'> */}
                  <article className="bringin-w-full bringin-max-h-[75vh] bringin-flex bringin-justify-center bringin-absolute bringin-bottom-0 xl:bringin-bottom-[0%] 2xl:bringin-bottom-[0%] 3xl:bringin-bottom-[0%] bringin-text-sm bringin-text-white">
                    {/* <div ref={descriptionBoxRef.current[index]} className='bringin-hidden bringin-w-2/5 sm:bringin-w-[46%] md:bringin-w-[58%] lg:bringin-w-[70%] xl:bringin-w-[64%] 2xl:bringin-w-[55%] 3xl:bringin-w-[70%] bringin-bg-black/80 bringin-h-full bringin-rounded-t-2xl bringin-pt-4 bringin-pb-20 bringin-px-4'> */}
                    <div
                      ref={descriptionBoxRef.current[index]}
                      className="bringin-hidden bringin-w-2/5 bringin-bg-black/80 bringin-rounded-t-2xl bringin-pt-4 bringin-pb-[30px] bringin-px-4"
                    >
                      <div className="bringin-flex bringin-justify-between bringin-items-center bringin-pb-2 bringin-border-2 bringin-border-solid bringin-border-transparent bringin-border-b-white  bringin-pr-3 bringin-font-bold">
                        <div></div>
                        <div className="bringin-tracking-wide bringin-text-lg">Shop</div>
                        <button
                          className="bringin-cursor-pointer bringin-border-none bringin-bg-transparent bringin-text-white bringin-text-xl"
                          onClick={handleCloseDesc}
                        >
                          <FontAwesomeIcon icon={faXmark} />
                        </button>
                      </div>
                      <a
                        className="bringin-w-full bringin-no-underline bringin-text-white"
                        href={`/products/${video?.products[selectedProductIndex]?.handle}`}
                        onClick={handleRedirectProductDespcriptionPage}
                        target="_blank"
                        rel="noreferrer"
                      >
                        <div className="bringin-flex bringin-items-center bringin-gap-2 bringin-text-sm bringin-pt-2 bringin-font-bold">
                          <img
                            src={
                              video?.products[selectedProductIndex]?.images[0]
                                ?.src
                            }
                            alt={video?.products[selectedProductIndex]?.title}
                            className="bringin-rounded"
                            width="50px"
                            height="50px"
                          />
                          <div>
                            {video?.products[selectedProductIndex]?.title}{" "}
                            <br />
                            {`₹ ${video?.products[selectedProductIndex]?.variants[selectedVariantIndex]?.price}`}
                          </div>
                        </div>
                      </a>
                      <section
                        className="bringin-transition  bringin-duration-150 bringin-ease-out"
                        style={{
                          display: toggleDescription ? "none" : "block",
                        }}
                      >
                        <div className="bringin-pt-2 bringin-text-sm">
                          <div className="bringin-font-bold bringin-mb-2">Description</div>
                          <div
                            ref={descriptionTextRef}
                            className={`bringinDescriptionBox bringin-transition-height bringin-text-white bringin-duration-200 bringin-ease-linear  bringin-text-justify
                              ${
                                moreDes
                                  ? "bringin-max-h-52 bringin-overflow-y-scroll"
                                  : "bringin-max-h-16 bringin-overflow-y-hidden"
                              }`}
                            dangerouslySetInnerHTML={{
                              __html:
                                video?.products[selectedProductIndex]
                                  ?.description,
                            }}
                          ></div>
                        </div>
                        <div
                          className="bringin-text-center bringin-cursor-pointer bringin-mt-4 bringin-font-bold hover:bringin-text-white/80"
                          onClick={() => setMoreDes((prev) => !prev)}
                        >
                          {moreDes ? "Less" : "More"}
                        </div>
                        {varinatSelected ? (
                          <div className="bringin-w-full bringin-flex bringin-gap-4">
                            <a
                              className={`${
                                checkoutEnabled === "true" ? "bringin-w-1/2" : "bringin-w-full"
                              } bringin-no-underline`}
                              onClick={handleRedirectProductDespcriptionPage}
                              href="/cart"
                            >
                              <button
                                id="bringinAddedToCart"
                                className="bringin-w-full bringin-cursor-pointer bringin-border-none bringin-bg-white bringin-text-black  bringin-mt-2 bringin-mb-2 hover:bringin-bg-white/90"
                              >
                                <FontAwesomeIcon
                                  color="#198754"
                                  icon={faCircleCheck}
                                />{" "}
                                Added to Cart
                              </button>
                            </a>
                            {checkoutType === "gokwik" ? (
                              <div className="bringin-gokwik-checkout bringin-mb-0 bringin-w-1/2">
                                <button
                                  id="bringinCheckout"
                                  type="button"
                                  className="bringin-w-full bringin-cursor-pointer bringin-border-none bringin-mt-2 bringin-mb-2 bringin-relative"
                                  ref={checkoutGokwikRef}
                                  // onClick= "onCheckoutClick(this)"
                                  onClick={() =>
                                    window.onCheckoutClick(
                                      checkoutGokwikRef.current
                                    )
                                  }
                                >
                                  <span className="">
                                    <span>
                                      Checkout{" "}
                                      <FontAwesomeIcon icon={faCartShopping} />{" "}
                                      <span
                                        id="cartCountBadge"
                                        style={{
                                          top: "-13px",
                                          right: "-12px",
                                        }}
                                        className="bringin-absolute bringin-rounded-full  bringin-w-7 bringin-h-7 bringin-flex bringin-justify-center bringin-items-center bringin-right-0 bringin-border bringin-border-white"
                                      >
                                        {cartCount}
                                      </span>
                                    </span>
                                    <span></span>
                                  </span>
                                  <span className="bringin-pay-opt-icon bringin-hidden">
                                    {/* <img src="https://cdn.gokwik.co/v4/images/upi-icons.svg" />
                                      <img src="https://cdn.gokwik.co/v4/images/right-arrow.svg" /> */}
                                  </span>
                                  <div className="bringin-hidden">
                                    <div className="bringin-cir-loader">Loading..</div>
                                  </div>
                                </button>
                              </div>
                            ) : (
                              <a
                                className={`${
                                  checkoutEnabled === "true"
                                    ? "bringin-w-1/2"
                                    : "bringin-hidden"
                                } bringin-no-underline`}
                                href="/checkout"
                              >
                                <button
                                  id="bringinCheckout"
                                  className="bringin-w-full bringin-cursor-pointer bringin-border-none bringin-mt-2 bringin-mb-2 bringin-relative"
                                >
                                  Checkout{" "}
                                  <FontAwesomeIcon icon={faCartShopping} />{" "}
                                  <span
                                    id="cartCountBadge"
                                    style={{
                                      top: "-13px",
                                      right: "-12px",
                                    }}
                                    className="bringin-absolute bringin-rounded-full  bringin-w-7 bringin-h-7 bringin-flex bringin-justify-center bringin-items-center bringin-right-0 bringin-border bringin-border-white"
                                  >
                                    {cartCount}
                                  </span>
                                </button>
                              </a>
                            )}
                          </div>
                        ) : !varinatSelected && addToCartEnabled === "true" ? (
                          <button
                            id="bringinAddToCart"
                            className="bringin-w-full bringin-cursor-pointer bringin-border-none bringin-mt-2 bringin-mb-2"
                            onClick={(event) => handleAddToCart(event, index)}
                          >
                            Add to Cart
                          </button>
                        ) : (
                          ""
                        )}
                      </section>
                      <section
                        style={{
                          display: !toggleDescription ? "none" : "block",
                        }}
                        className="bringin-mt-4"
                      >
                        <article className="bringin-flex bringin-justify-center bringin-flex-wrap bringin-gap-3">
                          {video?.products[selectedProductIndex]?.variants?.map(
                            (variant, idt) => {
                              return (
                                <div key={variant?.id} className="bringin-mt-2">
                                  <input
                                    type="radio"
                                    value={variant.variant_id}
                                    className="btnVariantRadio bringin-hidden"
                                    name={`btnradio_${index}`}
                                    id={`btnradio_${index}_${variant.id}`}
                                    onChange={(event) =>
                                      handleVariantChange(event, variant, idt)
                                    }
                                  />
                                  <label
                                    className="btnVariant bringin-cursor-pointer"
                                    htmlFor={`btnradio_${index}_${variant.id}`}
                                  >
                                    {variant.title}
                                  </label>
                                </div>
                              );
                            }
                          )}
                        </article>
                        <div className="bringin-flex bringin-justify-center bringin-mt-3">
                          <button
                            className=" bringin-w-fit bringin-cursor-pointer bringin-text-black bringin-p-2 bringin-text-base bringin-bg-white bringin-border-none bringin-rounded bringin-mt-2 bringin-mb-2 bringin-px-5 bringin-py-2  hover:bringin-bg-white/90"
                            onClick={() => handleDone(index)}
                          >
                            Done
                          </button>
                        </div>
                      </section>
                    </div>
                  </article>

                  <div
                    ref={playButtonRef.current[index]}
                    className="bringin-hidden bringin-text-8xl xl:bringin-text-[10rem] bringin-text-white/40 bringin-absolute bringin-top-2/4  bringin-left-2/4 bringin-translate-x-[-50%] bringin-translate-y-[-50%]"
                    onClick={() => handlePausePlayVideo(index)}
                  >
                    <FontAwesomeIcon icon={faPlay} />
                  </div>
                </div>
              </swiper-slide>
            );
          })}
        </swiper-container>
        <article
          id="productSwiper"
          style={{
            zIndex: 1,
            transform: "translateX(-50%)",
            width: `${videoWidth}px`,
          }}
          className="bringin-absolute bringin-bottom-[2%] bringin-left-2/4 "
        >
          <swiper-container
            className="mySwiper"
            ref={ProductSwiperRef}
            navigation="true"
            centered-slides="true"
            init="false"
          >
            {productVideoResponse[currentIndex]?.products?.map(
              (prod, index) => {
                const obj = {
                  id: productVideoResponse[currentIndex]?.id,
                  title: productVideoResponse[currentIndex]?.title,
                };
                return (
                  <swiper-slide
                    style={productModalStyle.products}
                    key={prod?.id}
                    data-videoDetails={JSON.stringify(obj)}
                    onClick={(event) => handleOpenDescription(event, index)}
                  >
                    <img
                      className="bringin-w-[60px] bringin-h-[60px] bringin-rounded bringin-object-cover "
                      alt={prod?.images[0]?.alt}
                      src={prod?.images[0]?.src}
                    />
                    <div className="bringin-flex bringin-flex-col bringin-justify-between bringin-w-[60%] xxlg:bringin-w-[63%] 5xl:bringin-w-[70%] bringin-text-[14px] ">
                      <div className="prodTitle bringin-overflow-hidden bringin-whitespace-nowrap bringin-text-ellipsis ">
                        {prod?.title}
                      </div>
                      <div>{`₹ ${prod?.variants[0]?.price} `}</div>
                      <div>Shop Now {`>`}</div>
                    </div>
                  </swiper-slide>
                );
              }
            )}
          </swiper-container>
        </article>
      </section>
    </Modal>
  );
}

export default SwiperModal;
