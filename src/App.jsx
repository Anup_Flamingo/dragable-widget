import { useEffect, useState, useRef, createRef } from "react";
import verge from "verge";
import WaitForElementToDisplay from "./Methods/MountUI";
import axios from "axios";
import Draggable from "react-draggable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmarkCircle } from "@fortawesome/free-regular-svg-icons";
import { faPlay } from "@fortawesome/free-solid-svg-icons";
import SwiperModal from "./Component/SwiperModal";
import { PRODUCT_URL, VIDEO_URL, PLAUSIBLE_DOMAIN, API_HOST, Thumbnail_URL, BRINGIN_URL } from "./config";
import FindElementPos from "./Methods/FindElementPos";
import Plausible from "plausible-tracker";
import "./App.css";

// Testing Mode
// import "./Style/DummyStyle.css";
// Testing Mode

const { trackEvent, enableAutoPageviews } = Plausible({
  domain: PLAUSIBLE_DOMAIN,
  apiHost: API_HOST,
  // Start: Testing Mode
  // trackLocalhost: true,
  // End: Testing Mode
});

const initialPosition = {
  prevLeft: -1,
  prevTop: -1,
  left: 0,
  top: 0,
};

function App() {
  const [mounted, setMounted] = useState(false);
  const [viewportHeigth, setViewportHeight] = useState(verge?.viewportH());
  const [productVideoResponse, setProductVideResponse] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentVideo, setCurrentVideo] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [showThumbnail, setShowThumbnail] = useState(true);
  const [dragging, setDragging] = useState(false);
  const [position, setPosition] = useState(initialPosition);


  const [throughDraggable, setThroughDraggable] = useState(false);
  const [throughSlider, setThroughSlider] = useState(false);

  // Start: Testing Mode
  // const [productId, setProductId] = useState("7766043328666");
  // End: Testing Mode

  // Start: Production Mode
  const [productId, setProductId] = useState(null);
  // End: Production Mode
  const [checkoutEnabled, setCheckoutEnabled] = useState("true");
  const [addToCartEnabled, setAddToCartEnabled] = useState("true");
  const [checkoutType, setCheckoutType] = useState("shopify");
  const [dragProdSlider, setDragProdSlider] = useState("both");
  const [draggableSize, setDraggableSize] = useState("small")

  const controlsVideoRef = useRef([]);
  const descriptionBoxRef = useRef([]);
  const nodeRef = useRef(null);
  const swiperElRef = useRef();
  const VideoSwiperRef = useRef([]);

  if(VideoSwiperRef.current.length ===0){
    VideoSwiperRef.current = productVideoResponse?.map((item) =>
      createRef()
    );
  }
  

  const handleMouseDown = (event) => {
    // console.log("Popup Interaction");
    // console.log({
    //   store: window.location.hostname,
    //   productId,
    // });
    trackEvent("Popup Interaction", {
      props: {
        store: window.location.hostname,
        productId,
      },
    });
  };

  useEffect(() => {

    WaitForElementToDisplay(
      ".maProductPopUp",
      function () {
        setMounted(true);
      },
      100,
      5000
    );

    visualViewport.addEventListener("resize", (event) => {
      setViewportHeight(verge?.viewportH());
    });
  }, []);

  useEffect(() => {
    if (mounted) {
      setProductId(document.querySelector(".maProductPopUp").dataset.productid);
      setCheckoutEnabled(
        document.querySelector(".maProductPopUp").dataset.checkoutenabled
      );
      setAddToCartEnabled(
        document.querySelector(".maProductPopUp").dataset.addtocartenabled
      );
      setCheckoutType(
        document.querySelector(".maProductPopUp").dataset.checkouttype
      );
      setDragProdSlider(
        document.querySelector(".maProductPopUp").dataset.dragprodslidertype
        )
        
        setDraggableSize(
        document.querySelector(".maProductPopUp").dataset.draggablesize
      )

      
      const customClass = document.querySelector(".maProductPopUp").dataset.customclass;
      if(document.getElementById('productRoot') && customClass){
        
        // const customClass = "Anup";
        document.getElementById('productRoot').classList.add(customClass);
      }

    }
  }, [mounted]);

  useEffect(() => {
    if (productId) {
      axios.get(`${PRODUCT_URL}/${productId}`).then((res) => {
        setProductVideResponse([...res.data]);
        enableAutoPageviews();
        if(dragProdSlider === "draggable" || dragProdSlider === "both"){

          // console.log("View Popup");
          // console.log({
          //   store: window.location.hostname,
          // productId,
          // });
            trackEvent("View Popup", {
              props: {
                store: window.location.hostname,
                productId,
              },
            });
        }

        if(dragProdSlider === "prodSlider" || dragProdSlider === "both"){
          //  console.log("View PDP");
          // console.log({
          //   store: window.location.hostname,
          //   productId,
          // }); 
        trackEvent("View PDP", {
          props: {
            store: window.location.hostname,
            productId,
          },
        });
        }

        descriptionBoxRef.current = res.data?.map((item) => createRef());

        controlsVideoRef.current = res.data?.map((item) => createRef());
      });
    }
  }, [productId]);

  useEffect(() => {
    // console.log(position);
    if (
      position.prevLeft === position.left &&
      position.prevTop === position.top
    ) {
      handleOpenModal();
    }
  }, [position]);

  useEffect(() => {
    if (nodeRef.current) {
      const { top, left } = FindElementPos(nodeRef.current);
      const newPosition = {
        prevLeft: 0,
        prevTop: 0,
        left,
        top,
      };
      setPosition({ ...newPosition });
    }

    if (swiperElRef.current) {
      const swiperParams = {
        breakpoints: {
          0: {
            slidesPerView: 2,
            // spaceBetween: "auto",
          },
          570: {
            slidesPerView: 2,
            // spaceBetween: 20,
          },
          850: {
            slidesPerView: 3,
            // spaceBetween: 20,
          },
          1110: {
            slidesPerView: 4,
            // spaceBetween: 10,
          },
          1500: {
            slidesPerView: 5,
          },
          1920: {
            slidesPerView: 6,
          },
          2550: {
            slidesPerView: 7,
          },
        },
        on: {
          init() {
            // ...
          },
          slideChangeTransitionEnd: function () {
            console.log("PDP Slider Click");
            console.log({
              store: window.location.hostname,
              productId,
            });
            trackEvent("PDP Slider Click", {
              props: {
                store: window.location.hostname,
                productId,
              },
            });
          },
        },
      };

      // now we need to assign all parameters to Swiper element
      Object.assign(swiperElRef.current, swiperParams);

      // and now initialize it
      swiperElRef.current.initialize();

      if (
        document
          .querySelector("#parentSwiperSection  swiper-container")
          ?.shadowRoot?.querySelector(".swiper-button-prev") &&
        document
          .querySelector("#parentSwiperSection  swiper-container")
          ?.shadowRoot?.querySelector(".swiper-button-next")
      ) {
        document
          .querySelector("#parentSwiperSection  swiper-container")
          .shadowRoot.querySelector(".swiper-button-prev").style.color =
          "white";
        document
          .querySelector("#parentSwiperSection  swiper-container")
          .shadowRoot.querySelector(".swiper-button-next").style.color =
          "white";
      }
    }
  }, [productVideoResponse]);

  const handleOpenModal = () => {
    // console.log("Open Modal");
    if (!dragging) {
      VideoSwiperRef.current.forEach((videoRef) => {
        videoRef.current?.pause();
      });
      setShowThumbnail(false);
      setIsModalOpen(true);
      setThroughDraggable(true);
      setCurrentIndex(0)

      // console.log("View Popup");
      // console.log({
      //   store: window.location.hostname,
      //   productId,
      // });
      trackEvent("View Popup", {
        props: {
          store: window.location.hostname,
          productId,
        },
      });
    }
  };

  const handleCloseModal = () => {
    VideoSwiperRef.current.forEach((videoRef) => {
      videoRef.current?.play();
    });
    if(document.querySelector('.sliderMain')){
      document.querySelector('.sliderMain').style.zIndex = 1;
    }
    
    if (
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-prev") &&
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-next")
    ) {
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-prev").style.display =
        "block";
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-next").style.display =
        "block";
    }

    setCurrentIndex(0);
    setCurrentVideo(null);
    setIsModalOpen(false);
    document.body.style.overflow = "auto";
    if(throughDraggable){
      setShowThumbnail(true);
    }

    setThroughDraggable(false);
    setThroughSlider(false);
  };

  const handleOpenModalProductSlider = (video, index) => {
    setCurrentIndex(index);
    // Pausing all the background Thumbnail Video
    VideoSwiperRef.current.forEach((videoRef) => {
      videoRef.current?.pause();
    });
    swiperElRef.current.style.zIndex = -1;

    if (
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-prev") &&
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-next")
    ) {
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-prev").style.display = "none";
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-next").style.display = "none";
    }

    setThroughSlider(true);
    setIsModalOpen(true);
  };

  const handleCloseThumbnailVideo = (event) => {
    event.stopPropagation();
    // console.log("Thumbnail Close");
    setShowThumbnail(false);
  };

  const handleStartDragging = () => {
    setDragging(true);
  };

  const handleStopDragging = () => {
    if (nodeRef.current) {
      const { top, left } = FindElementPos(nodeRef.current);
      const newPosition = {
        prevLeft: position.left,
        prevTop: position.top,
        left,
        top,
      };
      setPosition({ ...newPosition });
    }
    // console.log("Dragged");
    setTimeout(() => {
      setDragging(false);
    }, 0);
  };

  return (
    // Start: Testing Mode
    // <div className="App text-red-700 ">
    // {/* End: Testing Mode   */}

    // {/* Start: Production Mode */}
    <div className="App">
      {productVideoResponse?.length > 0  && (dragProdSlider === "prodSlider" || dragProdSlider === "both") ? (
        <>
          <swiper-container
            ref={swiperElRef}
            class="sliderMain"
            init="false"
            navigation="true"
            space-between={10}
            // centered-slides="true"
          >
            {productVideoResponse?.map((video, index) => {
              return (
                <swiper-slide
                  style={{ display: "flex", justifyContent: "center" }}
                  key={video.id}
                >
                  <video
                    className="bringinvideo bringin-cursor-pointer bringin-block bringin-h-auto bringin-max-w-full bringin-object-contain bringin-overflow-clip"
                    ref={VideoSwiperRef.current[index]}
                    autoPlay
                    loop
                    muted
                    playsInline
                    preload="auto"
                    onClick={() => handleOpenModalProductSlider(video, index)}
                  >
                    <source
                      src={`${Thumbnail_URL}/${video.thumbnail}`}
                      type="video/mp4"
                    />
                  </video>
                </swiper-slide>
              );
            })}
          </swiper-container>
          <div className="bringin-text-right bringin-pr-2">
            <a
              className="bringin-text-black bringin-no-underline"
              href={BRINGIN_URL}
              target="_blank"
              rel="noreferrer"
            >
              Powered by Bringin
            </a>
          </div>
        </>
      ) : (
        ""
      )}

      {productVideoResponse?.length > 0 && showThumbnail && (dragProdSlider === "draggable" || dragProdSlider === "both") ? (
        <Draggable
          bounds="html"
          cancel=".bringinDragCloseBtn"
          nodeRef={nodeRef}
          onMouseDown={handleMouseDown}
          onDrag={handleStartDragging}
          onStop={handleStopDragging}
        >
          <div
            ref={nodeRef}
            className="bringinDragDiv bringin-z-[99999999999]  bringin-fixed bringin-right-[5px] bringin-bottom-[200px] bringin-cursor-pointer"
          >
            {/* <div className="bringin-h-40 bringin-w-24 bringin-box-border bringin-m-1"> */}
            <div className={`${draggableSize === "medium"?"bringin-h-[220px] bringin-w-[131px] ":draggableSize === "large"?"bringin-h-[380px] bringin-w-[200px] ":"bringin-h-[137px] bringin-w-[76px] "}  bringin-box-border bringin-m-1`}>
              <div
                style={{
                  boxShadow: "rgb(176, 176, 176) 0px 0px 10px 3px",
                  borderRadius: "10px",
                  overflow: "hidden",
                }}
                className="bringin-relative bringin-h-full"
              >
                <video
                  className="bringin-inline bringin-bg-white bringin-w-full bringin-h-full bringin-object-cover bringin-overflow-hidden"
                  autoPlay
                  loop
                  muted
                  playsInline
                  preload="auto"
                  src={`${VIDEO_URL}/${productVideoResponse[0]?.thumbnail}`}
                ></video>
              </div>

              <div className="bringin-relative bringin-block bringin-z-10 bringin-bottom-full bringin-w-full bringin-h-full">
                <div className="bringin-h-full bringin-flex bringin-flex-col bringin-justify-between">
                  <div className="bringin-flex bringin-flex-row-reverse bringin-cursor-pointer">
                    <FontAwesomeIcon
                      className="bringinDragCloseBtn bringin-bg-white bringin-text-blue-600 bringin-text-xl bringin-rounded-full fa-solid"
                      icon={faXmarkCircle}
                      onClick={handleCloseThumbnailVideo}
                    />
                  </div>
                  <div className="bringin-flex bringin-justify-center">
                    <FontAwesomeIcon
                      className="bringinDragPauseBtn bringin-text-white bringin-text-2xl"
                      icon={faPlay}
                    />
                  </div>
                  <div className="bringin-px-2">
                    <p className="bringin-text-white bringin-text-center bringin-text-sm bringin-rounded-sm bringin-leading-[110%]">
                      &nbsp;
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Draggable>
      ) : (
        ""
      )}
      {isModalOpen ? (
        <SwiperModal
          isModalOpen={isModalOpen}
          productId={productId}
          setIsModalOpen={setIsModalOpen}
          productVideoResponse={productVideoResponse}
          viewportHeigth={viewportHeigth}
          controlsVideoRef={controlsVideoRef}
          currentIndex={currentIndex}
          setCurrentIndex={setCurrentIndex}
          trackEvent={trackEvent}
          setShowThumbnail={setShowThumbnail}
          checkoutEnabled={checkoutEnabled}
          addToCartEnabled={addToCartEnabled}
          checkoutType={checkoutType}
          currentVideo={currentVideo}
          setCurrentVideo={setCurrentVideo}
          descriptionBoxRef={descriptionBoxRef}
          throughDraggable={throughDraggable}
          throughSlider={throughSlider}
          handleCloseModal={handleCloseModal}
        />
      ) : (
        ""
      )}
    </div>
  );
}

// fixed bottom-[129px] right-[5px]
export default App;
