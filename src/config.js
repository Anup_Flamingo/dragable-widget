export const PRODUCT_URL = "https://bringin-shoppable.lyxelandflamingotech.in/api/popup-video";
export const VIDEO_URL =  "https://bringin-shoppable.sgp1.cdn.digitaloceanspaces.com";
export const Thumbnail_URL = "https://bringin-shoppable.sgp1.cdn.digitaloceanspaces.com";
export const BRINGIN_URL = "https://apps.shopify.com/bringin?utm_source=powered_by-widget";


// Start: Testing Mode  
// export const PLAUSIBLE_DOMAIN = "bringin-reviews.test";
// End: Testing Mode

// Start: Production Mode
export const PLAUSIBLE_DOMAIN = "bringin-shoppable.lyxelandflamingotech.in";
// End: Production Mode
export const API_HOST = "https://analytics.lyxelandflamingotech.in";
