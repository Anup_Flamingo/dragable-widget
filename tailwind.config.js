/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  prefix: "bringin-",
  theme: {
    screens:{
      "sm":"280px",
      "md": "320px",
      "lg":"450px",
      "xxlg":"600px",
      "xl":"800px",
      "2xl":"900px",
      "3xl":"1024px",
      "4xl": "1300px",
      "5xl":"1600px",
      "6xl":"1800px"
    },
    extend: {},
  },
  plugins: [],
  corePlugins:{
    preflight: false
  }
}

